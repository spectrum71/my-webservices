"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from movie_theatre import urls as movie_theatre
from excel_generator import urls as excel_generator
from activity_log import urls as activity_log

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^movie-theatre/', include((movie_theatre, 'movie_theatre'), namespace='movie-theatre')),
    url(r'^activity_log/', include((activity_log, 'activity_log'), namespace='activity_log')),
    url(r'^make-excel/', include((excel_generator, 'excel_generator'), namespace='make-excel'))
]
