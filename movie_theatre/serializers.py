from rest_framework import serializers
from .models import Movie, Theatre, Seat, Schedule, Ticket, Member, Order

class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = ['movie_id', 'title', 'genre']

class TheatreSerializer(serializers.ModelSerializer):
    def get_capacity(self, obj):
        return obj.capacity

    capacity = serializers.SerializerMethodField()

    class Meta:
        model = Theatre
        fields = ['theatre_id', 'theatre_type', 'capacity']

class SeatSerializer(serializers.ModelSerializer):

    class Meta:
        model = Seat
        fields = ['seat_id', 'theatre_id']

class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Schedule
        fields = ['schedule_id', 'date', 'time_start', 'time_finish', 'movie_id', 'theatre_id']

    def validate(self, attrs):
        """
        Check that start is before finish.
        """
        if attrs['time_start'] >= attrs['time_finish']:
            raise serializers.ValidationError({"time_start": ["time_start must occur before time_finish"]})
        return attrs


class TicketSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ticket
        fields = ['ticket_id', 'price', 'seat_id', 'schedule_id']

class MemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ['email', 'name', 'gender', 'address', 'phone_number']

class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['ticket_id', 'member_email', 'order_date']
