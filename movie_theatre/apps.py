from django.apps import AppConfig


class MovieTheatreConfig(AppConfig):
    name = 'movie_theatre'
