from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Movie(models.Model):
    movie_id = models.CharField(max_length=10, primary_key=True)
    title = models.CharField(max_length=50)
    genre = models.CharField(max_length=20)

class Theatre(models.Model):
    CHOICES = (("reg", "Reguler"), ("pre", "Premiere"))

    theatre_id = models.CharField(max_length=10, primary_key=True)
    theatre_type = models.CharField(max_length=3, choices=CHOICES)

    @property
    def capacity(self):
        return self.seat_set.count()

class Seat(models.Model):
    seat_id = models.CharField(max_length=10, primary_key=True)
    theatre_id = models.ForeignKey(Theatre, on_delete=models.CASCADE)

class Schedule(models.Model):
    schedule_id = models.CharField(max_length=10, primary_key=True)
    date = models.DateField()
    time_start = models.TimeField()
    time_finish = models.TimeField()
    movie_id = models.ForeignKey(Movie, on_delete=models.CASCADE)
    theatre_id = models.ForeignKey(Theatre, on_delete=models.CASCADE)

class Ticket(models.Model):
    ticket_id = models.CharField(max_length=10, primary_key=True)
    price = models.PositiveIntegerField(
        validators=[MaxValueValidator(10000000),
                    MinValueValidator(10000)])
    seat_id = models.ForeignKey(Seat, on_delete=models.CASCADE)
    schedule_id = models.ForeignKey(Schedule, on_delete=models.CASCADE)

class Member(models.Model):
    CHOICES = (("M", "Male"), ("F", "Female"))

    email = models.EmailField(max_length=70, primary_key=True)
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=CHOICES)
    address = models.TextField()
    phone_number = models.CharField(max_length=12)

class Order(models.Model):
    ticket_id = models.OneToOneField(Ticket, primary_key=True, on_delete=models.CASCADE)
    member_email = models.ForeignKey(Member, blank = True, null=True, on_delete=models.SET_NULL)
    order_date = models.DateField()