from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from movie_theatre import views

urlpatterns = [
    path('movie/', views.MovieList.as_view()),
    path('movie/<str:pk>/', views.MovieDetail.as_view()),
    path('theatre/', views.TheatreList.as_view()),
    path('theatre/<str:pk>/', views.TheatreDetail.as_view()),
    path('seat/', views.SeatList.as_view()),
    path('seat/<str:pk>/', views.SeatDetail.as_view()),
    path('schedule/', views.ScheduleList.as_view()),
    path('schedule/<str:pk>/', views.ScheduleDetail.as_view()),
    path('ticket/', views.TicketList.as_view()),
    path('ticket/<str:pk>/', views.TicketDetail.as_view()),
    path('member/', views.MemberList.as_view()),
    path('member/<str:pk>/', views.MemberDetail.as_view()),
    path('order/', views.OrderList.as_view()),
    path('order/<str:pk>/', views.OrderDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
