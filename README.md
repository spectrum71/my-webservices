# My Webservices

Here are the list of my webservices

### 1. Movie Theatre Web Services
#### URL Address : https://my-webservices.herokuapp.com/movie-theatre/ (don't bother visit it directly. nothing there. use endpoints)
#### Endpoints (click the markdown path to try it yourself!)
- Movie path
  - GET all, delete all and POST entity : [movie/](https://my-webservices.herokuapp.com/movie-theatre/movie)
  - GET, PUT, and DELETE by pk : movie/{MVXXXX}
- Theatre path
  - GET all, delete all and POST entity : [theatre/](https://my-webservices.herokuapp.com/movie-theatre/theatre)
  - GET, PUT, and DELETE by pk : theatre/{THXXXX}
- Seat path
  - GET all, delete all and POST entity : [seat/](https://my-webservices.herokuapp.com/movie-theatre/seat)
  - GET, PUT, and DELETE by pk : seat/{STXXXX}
- Schedule path
  - GET all, delete all and POST entity : [schedule/](https://my-webservices.herokuapp.com/movie-theatre/schedule)
  - GET, PUT, and DELETE by pk : schedule/{SCXXXX}
- Ticket path
  - GET all, delete all and POST entity : [ticket/](https://my-webservices.herokuapp.com/movie-theatre/ticket)
  - GET, PUT, and DELETE by pk : ticket/{TKXXXX}
- Member path
  - GET all, delete all and POST entity : [member/](https://my-webservices.herokuapp.com/movie-theatre/member)
  - GET, PUT, and DELETE by pk : member/{email}
- Order path
  - GET all, delete all and POST entity : [order/](https://my-webservices.herokuapp.com/movie-theatre/order)
  - GET, PUT, and DELETE by pk : order/{TKXXXX}
