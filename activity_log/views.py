from .models import ActivityLog
from .serializers import ActivityLogSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
class ActivityLogList(APIView):
    """
    List or delete all movies, or create a new movie.
    """
    def get(self, request, format=None):
        activities = ActivityLog.objects.all()
        serializer = ActivityLogSerializer(activities, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ActivityLogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)