from rest_framework import serializers
from .models import ActivityLog

class ActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = ActivityLog
        fields = ['id', 'title', 'date', 'start', 'finish', 'timestamp', 'description']
