from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from activity_log import views

urlpatterns = [
    path('activity_log/', views.ActivityLogList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
