from django.db import models

# Create your models here.
class JsonStorage(models.Model):
    json_id = models.CharField(max_length=70, primary_key=True)
    json_content = models.TextField()

class ScheduleStorage(models.Model):
    json_id = models.CharField(max_length=70, primary_key=True)
    json_content = models.TextField()