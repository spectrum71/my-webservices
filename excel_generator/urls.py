from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from excel_generator import views

urlpatterns = [
    path('json-reg/', views.JsonRegister.as_view()),
    path('excel-gen/<str:pk>/', views.ExcelGenerator.as_view()),
    path('schedule-reg/', views.ScheduleRegister.as_view()),
    path('schedule-gen/<str:pk>/', views.ScheduleGenerator.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
