import json
from django.shortcuts import render
from .models import JsonStorage, ScheduleStorage
from .parsers import payload_to_excel
from django.http import Http404, HttpResponse
from rest_framework.response import Response
from rest_framework.views import APIView
from openpyxl import Workbook
from openpyxl.styles import Alignment, PatternFill
from openpyxl.styles.borders import Border, Side
from openpyxl.utils import get_column_letter
from datetime import datetime as dt
from openpyxl.writer.excel import save_virtual_workbook

# Create your views here.
class JsonRegister(APIView):
    """
    Create a new file content in json.
    """
    def post(self, request, format=None):
        content = request.data
        json_obj = JsonStorage.objects.create(json_id=generate_filename(),
                                        json_content=json.dumps(content))

        url_path = '/excel-gen/' + str(json_obj.json_id) + '/'

        return Response({'api_path':url_path})

class ScheduleRegister(APIView):
    """
    Create a new schedule file content in json.
    """
    def post(self, request, format=None):
        content = request.data
        json_obj = ScheduleStorage.objects.create(json_id=generate_filename(),
                                        json_content=json.dumps(content))

        url_path = '/schedule-gen/' + str(json_obj.json_id) + '/'

        return Response({'api_path':url_path})

class ExcelGenerator(APIView):
    """
    Generate registered json into excel file.
    """
    def get_object(self, pk):
        try:
            return JsonStorage.objects.get(json_id=pk)
        except JsonStorage.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        
        json_obj = self.get_object(pk)
        book = Workbook()
        sheet = book.active
        wrap_alignment = Alignment(wrapText=True, vertical='center')
        thin_border = Border(left=Side(style='thin'), 
                     right=Side(style='thin'), 
                     top=Side(style='thin'), 
                     bottom=Side(style='thin'))

        content = json.loads(json_obj.json_content)

        sheet.merge_cells(start_row=1, start_column=7,
                         end_row=2, end_column=7)

        sheet.cell(row=1, column=7).border = thin_border
        sheet.cell(row=2, column=7).border = thin_border
        
        sheet.merge_cells(start_row=3, start_column=7,
                         end_row=4, end_column=7)

        sheet.cell(row=3, column=7).border = thin_border
        sheet.cell(row=4, column=7).border = thin_border

        FIELD_ROW = 1
        column_pointer = 1
        for field in content['fields']:
            # sheet.merge_cells(start_row=FIELD_ROW, start_column=column_pointer,
            #                 end_row=FIELD_ROW, end_column=column_pointer + 1)
            sheet.column_dimensions[get_column_letter(column_pointer)].width = 50
            sheet.cell(row=FIELD_ROW, column=column_pointer).alignment = wrap_alignment
            sheet.cell(row=FIELD_ROW, column=column_pointer).border = thin_border
            sheet.cell(row=FIELD_ROW, column=column_pointer).value = field

            column_pointer += 1

        row_pointer = 2
        column_pointer = 1
        for obj in content['instances']:
            for field in content['fields']:
                # sheet.merge_cells(start_row=row_pointer, start_column=column_pointer,
                #             end_row=row_pointer + 1, end_column=column_pointer + 1)
                sheet.cell(row=row_pointer, column=column_pointer).alignment = wrap_alignment
                sheet.cell(row=row_pointer, column=column_pointer).border = thin_border
                sheet.cell(row=row_pointer, column=column_pointer).value = obj[field]
                column_pointer += 1
            column_pointer = 1
            row_pointer += 1
        
        response = HttpResponse(save_virtual_workbook(book), content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="%s.xlsx"' % (pk)
        return response

class ScheduleGenerator(APIView):
    """
    Generate registered schedule into excel file.
    """
    def get_object(self, pk):
        try:
            return ScheduleStorage.objects.get(json_id=pk)
        except ScheduleStorage.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        
        json_obj = self.get_object(pk)
        book = payload_to_excel(json.loads(json_obj.json_content))
        
        response = HttpResponse(save_virtual_workbook(book), content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="%s.xlsx"' % (pk)
        return response

def generate_filename():
    timestamp = dt.now()
    return "_".join(['file', str(timestamp.date()),
                str(timestamp.time())])