# Generated by Django 2.1.7 on 2019-05-04 16:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('excel_generator', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScheduleStorage',
            fields=[
                ('json_id', models.CharField(max_length=70, primary_key=True, serialize=False)),
                ('json_content', models.TextField()),
            ],
        ),
    ]
