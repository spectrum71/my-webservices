from openpyxl import Workbook
from openpyxl.styles import Alignment, PatternFill
from openpyxl.styles.borders import Border, Side
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook

def stringify_list(joiner, list_input):
    if not isinstance(list_input, list):
        raise TypeError('Tipe input harus dalam list')
    return joiner.join(map(str, list_input))

def stringify_class_room(dictio):
    if not isinstance(dictio, dict):
        raise TypeError('Tipe input harus dalam dictionary')
    class_room = list()
    try:
        for cl, rooms in dictio.items():
            class_room.append(cl + ": " + stringify_list(', ', rooms))

        return stringify_list('\n', class_room)
    except TypeError:
        raise TypeError('Pastikan format dictionary anda benar')

def __set_cell_value(sheet, cell_row, cell_col, cell_val):
    sheet.cell(
        row=cell_row, column=cell_col).alignment = Alignment(
            wrapText=True, vertical='center')
    sheet.cell(row=cell_row, column=cell_col).value = cell_val

def __border_cells(sheet, row_start, col_start, row_end, col_end):
    thin_border = Border(
        left=Side(style='thin'),
        right=Side(style='thin'),
        top=Side(style='thin'),
        bottom=Side(style='thin'))

    for i in range(row_start, row_end):
        for j in range(col_start, col_end):
            sheet.cell(row=i, column=j).border = thin_border


def payload_to_excel(excel_payload):
    if not isinstance(excel_payload, dict):
        raise TypeError('Payload harus berupa dictionary')
    SLOT_COLUMN = 1
    SLOT_TEXT_WIDTH = 20
    DATE_ROW = 3
    FIELD_ROW = 4
    DATA_ROW_START = 6
    DATA_COLUMN_START = 2
    DATA_TEXT_WIDTH = 15

    book = Workbook()
    sheet = book.active

    sheet.merge_cells(
        start_row=DATE_ROW,
        start_column=SLOT_COLUMN,
        end_row=FIELD_ROW + 1,
        end_column=SLOT_COLUMN)

    sheet.column_dimensions[get_column_letter(
        SLOT_COLUMN)].width = SLOT_TEXT_WIDTH
    __set_cell_value(sheet, DATE_ROW, SLOT_COLUMN, 'Waktu Pelaksanaan')

    row_pointer = DATA_ROW_START
    column_pointer = DATA_COLUMN_START
    row_checkpoint = DATA_ROW_START
    for time_slot, slot_schedule in excel_payload.items():
        most_courses = 0
        for courses in slot_schedule.values():
            if len(courses) > most_courses:
                most_courses = len(courses)
            for course, classes in courses.items():
                __set_cell_value(sheet, row_pointer, column_pointer, course)
                __set_cell_value(sheet, row_pointer, column_pointer + 1,
                                 stringify_class_room(classes))

                row_pointer += 1
            column_pointer += 2
            row_pointer = row_checkpoint

        prev_checkpoint = row_checkpoint
        row_checkpoint += most_courses
        row_pointer = row_checkpoint
        column_pointer = DATA_COLUMN_START

        sheet.merge_cells(
            start_row=prev_checkpoint,
            start_column=SLOT_COLUMN,
            end_row=row_checkpoint - 1,
            end_column=SLOT_COLUMN)

        __set_cell_value(sheet, prev_checkpoint, SLOT_COLUMN, time_slot)

    date_list = list(excel_payload.values())[0].keys()
    row_end = row_checkpoint
    col_end = SLOT_COLUMN + len(date_list) * 2 + 1

    sheet.merge_cells(
        start_row=DATE_ROW,
        start_column=SLOT_COLUMN + 1,
        end_row=DATE_ROW,
        end_column=SLOT_COLUMN + len(date_list) * 2)
    __set_cell_value(sheet, DATE_ROW, SLOT_COLUMN + 1, 'Tanggal Ujian')

    column_pointer = DATA_COLUMN_START
    for date_item in date_list:
        sheet.merge_cells(
            start_row=FIELD_ROW,
            start_column=column_pointer,
            end_row=FIELD_ROW,
            end_column=column_pointer + 1)
        sheet.cell(row=FIELD_ROW, column=column_pointer).value = date_item

        __set_cell_value(sheet, FIELD_ROW + 1, column_pointer, 'Mata Kuliah')
        sheet.column_dimensions[get_column_letter(
            column_pointer)].width = DATA_TEXT_WIDTH

        __set_cell_value(sheet, FIELD_ROW + 1, column_pointer + 1, 'Ruangan')
        sheet.column_dimensions[get_column_letter(column_pointer +
                                                  1)].width = DATA_TEXT_WIDTH

        column_pointer += 2

    __border_cells(sheet, DATE_ROW, SLOT_COLUMN, row_end, col_end)

    return book