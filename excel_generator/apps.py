from django.apps import AppConfig


class ExcelGeneratorConfig(AppConfig):
    name = 'excel_generator'
